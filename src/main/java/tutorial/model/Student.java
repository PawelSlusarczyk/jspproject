﻿package tutorial.model;

import java.util.List;

/**
 * Klasa reprezentująca dane o studencie.
 */
public class Student {
	private Integer id;
	private String name;
	private List<Subject> subjects;
	
	public Student(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}
}
