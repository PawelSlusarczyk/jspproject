package tutorial.model;

public class Proposition {

	private Integer id;
	private String name;
	private String email;
	private String databaseType;
	private String schemaName;
	private String size;
	private String encoding;
	//private String databaseType;
	private String mycomment;
	private String status;
	private Integer userId;
	
	//private S

	
	public Proposition(Integer id, String name, String email, String databaseType, String schemaName, String size, String encoding, String mycomment, String status, Integer userId ) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.databaseType = databaseType;
		this.schemaName = schemaName;
		this.size = size;
		this.encoding = encoding;
		this.mycomment = mycomment;
		this.status = status;
		this.userId = userId;
	}
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getDatabaseType() {
		return databaseType;
	}



	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
	}



	public String getSchemaName() {
		return schemaName;
	}



	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}



	public String getSize() {
		return size;
	}



	public void setSize(String size) {
		this.size = size;
	}



	public String getEncoding() {
		return encoding;
	}



	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}



	public String getMycomment() {
		return mycomment;
	}



	public void setMycomment(String mycomment) {
		this.mycomment = mycomment;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public Integer getUserId() {
		return userId;
	}



	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	

	//public List<Subject> getSubjects() {
		//return subjects;
	//}

	//public void setSubjects(List<Subject> subjects) {
		//this.subjects = subjects;
	//}

}
