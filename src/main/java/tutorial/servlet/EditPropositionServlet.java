package tutorial.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class EditPropositionServlet
 */
@WebServlet("/EditPropositionServlet")
public class EditPropositionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(EditPropositionServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditPropositionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession(true);
		String loggedUserName = (String) session.getAttribute("loggedUserName");
		if (loggedUserName != null){

			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
			request.getRequestDispatcher("/WEB-INF/jsp/accesDenied.jsp").forward(request, response);

		}else{

			response.sendRedirect("index");

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String databaseType = request.getParameter("databaseType");
		String schemaName = request.getParameter("schemaName");
		String size = request.getParameter("size");
		String encoding = request.getParameter("encoding");
		String mycomment = request.getParameter("mycomment");
		String status = request.getParameter("status");
		String userId = request.getParameter("userId");

		request.setAttribute("id", id);
		request.setAttribute("name", name);
		request.setAttribute("size", size);
		request.setAttribute("email", email);
		request.setAttribute("databaseType", databaseType);
		request.setAttribute("schemaName", schemaName);
		request.setAttribute("size", size);
		request.setAttribute("encoding", encoding);
		request.setAttribute("mycomment", mycomment);
		request.setAttribute("status", status);
		request.setAttribute("userId", userId);

		request.getRequestDispatcher("/WEB-INF/jsp/editProposition.jsp").forward(request, response);

		LOGGER.info("Pobrano dane do edycji");

	}

}
