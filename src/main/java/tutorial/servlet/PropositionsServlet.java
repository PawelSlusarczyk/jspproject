﻿package tutorial.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import tutorial.dao.DataAccessException;
import tutorial.dao.PropositionsDao;
import tutorial.dao.jdbc.JdbcPropositionsDao;
import tutorial.model.Proposition;

/**
 * Servlet implementation class PropositionsServlet
 */
@WebServlet("/PropositionsServlet")
public class PropositionsServlet extends HttpServlet {

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(PropositionsServlet.class);

	private PropositionsDao propositionsDao;

	public PropositionsServlet() {
		propositionsDao = new JdbcPropositionsDao();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		LOGGER.debug("Servlet processing request for listing propositions");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");

		PrintWriter out = response.getWriter();
		try {
			// pobranie danych z warstwy DAO
			Integer IsItAdmin = (Integer) request.getSession().getAttribute("loggedUserAdmin");
			Integer loggedUserId = (Integer) request.getSession().getAttribute("loggedUserId");

			List<Proposition> propositions = null;

			if (loggedUserId == null) {
				LOGGER.debug("log2");
				request.getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);

			} else if (IsItAdmin == 1) {
				propositions = propositionsDao.listPropositions();
				LOGGER.debug("wywo�ano funkcje dla admina");
			} else if (IsItAdmin == 0) {
				propositions = propositionsDao.listUserPropositions(loggedUserId);
				LOGGER.debug("wywo�ano funkcje dla usera");

			}

			
			// dodanie obiektów, które zostaną wykorzystane przy renderowaniu
			// strony JSP
			request.setAttribute("propositions", propositions);
			// przekierowanie przetwarzania do JSP
			request.getRequestDispatcher("/WEB-INF/jsp/propositions.jsp").forward(request, response);
		} catch (DataAccessException e) {
			LOGGER.warn("Error during processing: ", e);
			e.printStackTrace(out);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
