﻿package tutorial.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import tutorial.dao.DataAccessException;
import tutorial.dao.PropositionsDao;
import tutorial.dao.jdbc.JdbcPropositionsDao;

/**
 * Servlet implementation class ChangePropositionServlet
 */
@WebServlet("/ChangePropositionServlet")
public class ChangePropositionServlet extends HttpServlet {

	private PropositionsDao propositionsDao;
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(EditPropositionServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangePropositionServlet() {
		super();
		propositionsDao = new JdbcPropositionsDao();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = request.getParameter("id");
		LOGGER.info("chng pobrano id" + id);
		String name = request.getParameter("name");
		LOGGER.info("chng pobrano name = " + name);
		String email = request.getParameter("email");
		LOGGER.info("chng pobrano email" + email);
		String databaseType = request.getParameter("databaseType");
		LOGGER.info(" chng pobrano db" + databaseType);
		String schemaName = request.getParameter("schemaName");
		LOGGER.info(" chng pobrano schema = " + schemaName);
		String size = request.getParameter("size");
		LOGGER.info(" chng pobrano szie = " + size);
		String encoding = request.getParameter("encoding");
		LOGGER.info("chng pobrano encoding = " + encoding);
		String comment = request.getParameter("comment");
		LOGGER.info("chng pobrano comment = " + comment);
		String status = request.getParameter("status");
		LOGGER.info("chng pobrano status" + status);
		String userId = request.getParameter("userId");
		LOGGER.info("chng pobrano userId " + userId);

		int idInt = Integer.parseInt(id);
		int userIdInt = Integer.parseInt(userId);

		try {

			// modyfikacja danych poprzez DAO

			propositionsDao.editProposition(idInt, name, email, databaseType, schemaName, size, encoding, comment,
					status, userIdInt);
			// przekierowanie na listę studentów
			LOGGER.info("zaktualizowano baze danych");

			response.sendRedirect("propositions");
		} catch (DataAccessException e) {
			LOGGER.warn("Error during processing: ", e);
			e.printStackTrace(response.getWriter());
		}

	}

}
