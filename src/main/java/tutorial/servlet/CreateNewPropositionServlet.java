﻿package tutorial.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import tutorial.dao.DataAccessException;
import tutorial.dao.PropositionsDao;
import tutorial.dao.jdbc.JdbcPropositionsDao;
import tutorial.model.Proposition;

/**
 * Servlet implementation class CreateNewPropositionServlet
 */
@WebServlet("/CreateNewPropositionServlet")
public class CreateNewPropositionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	private static final Logger LOGGER = Logger.getLogger(CreateNewPropositionServlet.class);
	private PropositionsDao propositionsDao;

	public CreateNewPropositionServlet() {

		propositionsDao = new JdbcPropositionsDao();
		// super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(true);
		String loggedUserName = (String) session.getAttribute("loggedUserName");
		if (loggedUserName == null) {
			response.sendRedirect("index");
		} else {
			request.getRequestDispatcher("/WEB-INF/jsp/newProposition.jsp").forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		request.setCharacterEncoding("UTF-8");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String databaseType = request.getParameter("databaseType");
		String schema = request.getParameter("schema");
		String size = request.getParameter("size");
		String encoding = request.getParameter("encoding");
		String comment = request.getParameter("comment");
		String status = request.getParameter("status");

		try {

			// modyfikacja danych poprzez DAO
			HttpSession session = request.getSession(true);
			int loggedUserId = (int) session.getAttribute("loggedUserId");
			propositionsDao.addProposition(new Proposition(null, name, email, databaseType, schema, size, encoding,
					comment, status, loggedUserId));

			// przekierowanie na listę studentów
			response.sendRedirect("propositions");
		} catch (DataAccessException e) {
			LOGGER.warn("Error during adding proposition: ", e);
			e.printStackTrace(response.getWriter());
		}
	}

}
