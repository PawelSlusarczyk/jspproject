﻿package tutorial.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import tutorial.dao.DataAccessException;
import tutorial.dao.UsersDao;
import tutorial.dao.jdbc.JdbcUsersDao;
import tutorial.model.User;

/**
 * Servlet implementation class LoginServlet2
 */
@WebServlet("/LoginServlet2")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final org.apache.log4j.Logger LOGGER = Logger.getLogger(LoginServlet.class);
	private UsersDao usersDao = new JdbcUsersDao();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at:
		// ").append(request.getContextPath());
		request.setCharacterEncoding("UTF-8");
		// przekierowanie przetwarzania do JSP
		request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(true);

		// pobranie nazwy studenta do dodania z żądania
		String login = request.getParameter("login");
		LOGGER.info("Pobrano login" + login);
		String password = request.getParameter("password");
		LOGGER.info("Pobrano haslo" + password);

		try {
			User newUser = null;
			newUser = usersDao.check(login, password);
			String loginUserName = null;
			int loginError;
			if (newUser != null) {

				loginError = 1;
				loginUserName = login;
				session.setAttribute("loggedUserName", loginUserName);
				request.setAttribute("loginError", false);
				session.setAttribute("loggedUserId", newUser.getId());
				session.setAttribute("loggedUserAdmin", newUser.getAdmin());

				LOGGER.warn("login: " + login);
				LOGGER.warn("zalogowany user  " + newUser.getUsername());
				LOGGER.warn("zalogowany haslo  " + newUser.getPassword());
				LOGGER.warn("zalogowany id  " + newUser.getId());
				LOGGER.warn("zalogowany admin  " + newUser.getAdmin());

				response.sendRedirect("index");
			} else {
				loginError = 0;
				response.sendRedirect("login");
			}
		} catch (DataAccessException e) {
			response.sendRedirect("login");
		}
	}
}
