package tutorial.dao;

import java.util.List;

import tutorial.model.Proposition;

public interface PropositionsDao {


	public List<Proposition> listPropositions() throws DataAccessException;
	
	public Proposition addProposition(Proposition proposition) throws DataAccessException;
	
	public void editProposition(int id, String name, String email, String databaseType, String schema, String size, String encoding, String mycomment, String status, int userId) throws DataAccessException;
	
	//public void removeProsposition(int id) throws DataAccessException;
	
	//public Proposition getProposition(int id) throws DataAccessException;
	
	public List<Proposition> listUserPropositions(int userId) throws DataAccessException;
}
