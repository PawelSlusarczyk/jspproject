package tutorial.dao;

import tutorial.model.User;

public interface UsersDao {
	
	public User check(String user, String password) throws DataAccessException;
	
}
