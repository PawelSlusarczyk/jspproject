﻿package tutorial.dao;

/**
 * Ogólny wyjątek oznaczający błąd w dostępie do danych.
 * Przykrywa błędy charakterystyczne dla konkretnej implementacji
 * np. SQLException, który występuje tylko przy JDBC
 */
public class DataAccessException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataAccessException(String string) {
		super(string);
	}

	public DataAccessException(String string, Throwable reason) {
		super(string, reason);
	}

}
