package tutorial.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import tutorial.dao.DataAccessException;
import tutorial.dao.PropositionsDao;
import tutorial.model.Proposition;

public class JdbcPropositionsDao implements PropositionsDao {
	
	private static final Logger LOGGER = Logger.getLogger(JdbcPropositionsDao.class);
	
	public List<Proposition> listPropositions() throws DataAccessException {
		LOGGER.debug("Listing propositions.");
		try(Connection connection = ConnectionProvider.getConnection()) {
			ResultSet rs = connection.createStatement().executeQuery("SELECT id, name, email, databaseType, schema, size, encoding, mycomment, status, userId FROM propositions");
			List<Proposition> result = new LinkedList<>();
			while(rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String email = rs.getString(3);
				String databaseType = rs.getString(4);
				String schemaName = rs.getString(5);
				String size = rs.getString(6);
				String encoding = rs.getString(7);
				String mycomment = rs.getString(8);
				String status = rs.getString(9);
				int userId = rs.getInt(10);
						
				
				result.add(new Proposition(id,name, email, databaseType, schemaName, size, encoding, mycomment, status, userId));
			}
			return result;
		} catch (SQLException e) {
			throw new DataAccessException("SQL exception while listing students", e);
		}
	}
	



	@Override
	public Proposition addProposition(Proposition proposition) throws DataAccessException {
		
		LOGGER.info("Adding new proposition: "+proposition.getName());
		
		try(Connection connection = ConnectionProvider.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement("INSERT INTO propositions(name, email, databaseType, schema, size, encoding, mycomment, status, userID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? )");
			stmt.setString(1, proposition.getName());
			stmt.setString(2, proposition.getEmail());
			stmt.setString(3, proposition.getDatabaseType());
			stmt.setString(4, proposition.getSchemaName());
			stmt.setString(5, proposition.getSize());
			stmt.setString(6, proposition.getEncoding());
			stmt.setString(7, proposition.getMycomment());
			stmt.setString(8, proposition.getStatus());
			stmt.setInt(9, proposition.getUserId());
			
		
		
			stmt.execute();
			ResultSet generatedIds = stmt.getGeneratedKeys();
			if(generatedIds.next()) {
				proposition.setId(generatedIds.getInt(1));
			}
			
			LOGGER.info("Adding new proposition: "+proposition.getId()); 
			
			return proposition;
		} catch (SQLException e) {
			throw new DataAccessException("SQL exception while adding new proposition "+proposition.getName(), e);
		}
		
	}
	
	public void editProposition(int id, String name, String email, String databaseType, String schema, String size, String encoding, String mycomment, String status, int userId) throws DataAccessException{
		
		
		LOGGER.info("editing proposition which has id: "+id);
		try(Connection connection = ConnectionProvider.getConnection()) {
			PreparedStatement stmt1 = connection.prepareStatement("UPDATE PROPOSITIONS SET name = ? , email = ?, databaseType = ?, schema = ?, size = ?, encoding = ?, mycomment = ?, status = ?, userId = ? WHERE id =  ? ");
			stmt1.setString(1, name);
			stmt1.setString(2, email );
			stmt1.setString(3, databaseType );
			stmt1.setString(4, schema );
			stmt1.setString(5, size);
			stmt1.setString(6, encoding);
			stmt1.setString(7, mycomment);
			stmt1.setString(8, status);
			stmt1.setInt(9, userId);
			stmt1.setInt(10, id);
			stmt1.execute();
			
		} catch (SQLException e) {
			throw new DataAccessException("SQL exception while updateting propositions "+id, e);
		}
		
		
	}
	
	public List<Proposition> listUserPropositions(int idOfUser) throws DataAccessException {
		LOGGER.debug("Listing propositions.");
		try(Connection connection = ConnectionProvider.getConnection()) {
			ResultSet rs = connection.createStatement().executeQuery("SELECT id, name, email, databaseType, schema, size, encoding, mycomment, status, userId FROM propositions WHERE userId = " + idOfUser );
			List<Proposition> result = new LinkedList<>();
			while(rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String email = rs.getString(3);
				String databaseType = rs.getString(4);
				String schemaName = rs.getString(5);
				String size = rs.getString(6);
				String encoding = rs.getString(7);
				String mycomment = rs.getString(8);
				String status = rs.getString(9);
				int userId = rs.getInt(10);
						
				
				result.add(new Proposition(id,name, email, databaseType, schemaName, size, encoding, mycomment, status, userId));
			}
			return result;
		} catch (SQLException e) {
			throw new DataAccessException("SQL exception while listing students", e);
		}
	}
	

}



