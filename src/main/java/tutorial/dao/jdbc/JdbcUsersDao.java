package tutorial.dao.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import tutorial.dao.DataAccessException;
import tutorial.dao.UsersDao;
import tutorial.model.User;

public class JdbcUsersDao implements UsersDao {

	private static final Logger LOGGER = Logger.getLogger(JdbcPropositionsDao.class);

	@Override

	public User check(String user, String password) throws DataAccessException {
		// TODO Auto-generated method stub

		LOGGER.info("checking if the given password is ok");
		try (Connection connection = ConnectionProvider.getConnection()) {
			ResultSet rs = connection.createStatement()
					.executeQuery("SELECT id, username, password, isItAdmin FROM USERS");
			while (rs.next()) {
				int idDb = rs.getInt(1);
				String usernameDb = rs.getString(2);
				String passwordDb = rs.getString(3);
				int IsItAdminDb = rs.getInt(4);
				LOGGER.info("userdb = " + usernameDb + ", user" + user + ",  passworddb  = " + passwordDb
						+ " , password = " + password);

				if ((usernameDb.equals(user)) && (passwordDb.equals(password))) {
					LOGGER.info("haslo sie zgadza");
					LOGGER.warn("ustawiono loginUserName na " + user);
					User userReturned = new User(idDb, user, password, IsItAdminDb);
					return userReturned;
				}
				LOGGER.info("haslo sie nie zgadza");
			}
			return null;
		} catch (SQLException e) {
			throw new DataAccessException("SQL exception while listing students", e);
		}
	}

}
