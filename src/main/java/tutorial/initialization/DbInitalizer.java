package tutorial.initialization;

import java.sql.Connection;
import java.sql.SQLException;

import tutorial.dao.jdbc.ConnectionProvider;

/**
 * Klasa inicjalizująca bazę danych przykładowymi danymi
 */
public class DbInitalizer {
	
	public static void init() throws SQLException, ClassNotFoundException {
		Class.forName("org.h2.Driver");
		try(Connection conn = ConnectionProvider.getConnection()) {
				
			execute(conn, "DROP TABLE IF EXISTS PROPOSITIONS");
			execute(conn, "DROP TABLE IF EXISTS USERS");
					
			execute(conn, "CREATE TABLE USERS (id INTEGER PRIMARY KEY auto_increment, username VARCHAR(1023), password VARCHAR(1023), isItAdmin INTEGER)");
			execute(conn, "CREATE TABLE PROPOSITIONS (id INTEGER PRIMARY KEY auto_increment, name VARCHAR(1023), email VARCHAR(1023), databaseType VARCHAR(1023), schema VARCHAR(1023), size VARCHAR(1023), encoding VARCHAR(1023), mycomment VARCHAR(1023), status VARCHAR(1023), userId INTEGER, " 
			+ "FOREIGN KEY (userId) REFERENCES users(id))");
								
			execute(conn, "INSERT INTO USERS VALUES(1,'user1', 'password1', 0)");
			execute(conn, "INSERT INTO USERS VALUES(2,'user2', 'password2', 0)");
			execute(conn, "INSERT INTO USERS VALUES(3,'user3', 'password3', 1)");
			
			execute(conn, "INSERT INTO PROPOSITIONS VALUES(1,'name1', 'email1' , 'dbtype1' , 'schema1', 'sieze1',  'encoding1', 'comment1', 'stat1', 1)");
			execute(conn, "INSERT INTO PROPOSITIONS VALUES(2,'name2', 'email2' , 'dbtype2' , 'schema1', 'sieze2',  'encoding2', 'comment2' , 'stat2', 2)");
			execute(conn, "INSERT INTO PROPOSITIONS VALUES(3,'name3', 'email3' , 'dbtype3' , 'schema3', 'sieze3',  'encoding3', 'comment3' , 'stat3', 3)");
			execute(conn, "INSERT INTO PROPOSITIONS VALUES(4,'name4', 'email4' , 'dbtype4' , 'schema4', 'sieze4',  'encoding4', 'comment4' , 'stat4', 4)");			
			
		}
	}
	
	private static void execute(Connection conn, String sql) throws SQLException {
		conn.createStatement().executeUpdate(sql);
	}
}
