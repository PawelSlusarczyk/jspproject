﻿package tutorial.initialization;

import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

/**
 * Po uruchomieniu aplikacji, wywołuje inicjalizację bazy danych
 */
public class InitListener implements ServletContextListener {
	
	private static final Logger LOGGER = Logger.getLogger(InitListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			DbInitalizer.init();
			LOGGER.info("Database initalized!");
		} catch (ClassNotFoundException | SQLException e) {
			LOGGER.error("Error on initializing the database", e);
		}
	}

}
