<%@ page pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="tutorial.model.Proposition"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<style>
table, th, td {
	border: 1px solid black;
}
</style>

</head>
<%
	List<Proposition> propositions = (List<Proposition>) (request.getAttribute("propositions"));
	int loggedUserAdmin = (int) session.getAttribute("loggedUserAdmin");
	Boolean loginError = (Boolean) request.getAttribute("loginError");
%>
<body>
	<h2>Wnioski</h2>
	<table>
		<tr>
			<td>Id</td>
			<td>Nazwa</td>
			<td>Typ bazy danych</td>
			<td>Email</td>
			<td>Nazwa schematu</td>
			<td>Rozmiar</td>
			<td>Kodowanie</td>
			<td>Komentarz</td>
			<td>Status</td>

			<% if ((loggedUserAdmin == 1)) {
			%> 
			<td>użytkownik</td>
			<td>Edycja</td> <% } %>
			<td>Szczegóły</td>




		</tr>
		<%
			for (Proposition proposition : propositions) {
		%>
		<tr>
			<td><%=proposition.getId()%></td>
			<td><%=proposition.getName()%></td>
			<td><%=proposition.getDatabaseType()%></td>
			<td><%=proposition.getEmail()%></td>
			<td><%=proposition.getSchemaName()%></td>
			<td><%=proposition.getSize()%></td>
			<td><%=proposition.getEncoding()%></td>
			<td><%=proposition.getMycomment()%></td>
			<td><%=proposition.getStatus()%></td>
			
			
			<% if ((loggedUserAdmin == 1)) {
			%> 
			
			<td><%=proposition.getUserId()%></td>
			<% } %>



			<%
				if ((loggedUserAdmin == 1)) {
			%>
			<td>
				<form action="edit-proposition" method="post">
					<input type="hidden" name="id" value="<%=proposition.getId()%>" />
					<input type="hidden" name="name"
						value="<%=proposition.getName()%>" /> <input type="hidden"
						name="databaseType" value="<%=proposition.getDatabaseType()%>" />
					<input type="hidden" name="email"
						value="<%=proposition.getEmail()%>" /> <input type="hidden"
						name="schemaName" value="<%=proposition.getSchemaName()%>" /> <input
						type="hidden" name="size" value="<%=proposition.getSize()%>" />
					<input type="hidden" name="encoding"
						value="<%=proposition.getEncoding()%>" /> <input type="hidden"
						name="mycomment" value="<%=proposition.getMycomment()%>" /> <input
						type="hidden" name="status" value="<%=proposition.getStatus()%>" />
					<input type="hidden" name="userId"
						value="<%=proposition.getUserId()%>" /> <input type="submit"
						value="Edytuj wniosek" />
				</form>
			</td>
			<%
				}
			%>

			<td>
				<form action="proposition-details" method="post">
					<input type="hidden" name="id" value="<%=proposition.getId()%>" />
					<input type="hidden" name="name"
						value="<%=proposition.getName()%>" /> <input type="hidden"
						name="databaseType" value="<%=proposition.getDatabaseType()%>" />
					<input type="hidden" name="email"
						value="<%=proposition.getEmail()%>" /> <input type="hidden"
						name="schemaName" value="<%=proposition.getSchemaName()%>" /> <input
						type="hidden" name="size" value="<%=proposition.getSize()%>" />
					<input type="hidden" name="encoding"
						value="<%=proposition.getEncoding()%>" /> <input type="hidden"
						name="mycomment" value="<%=proposition.getMycomment()%>" /> <input
						type="hidden" name="status" value="<%=proposition.getStatus()%>" />
					<input type="hidden" name="userId"
						value="<%=proposition.getUserId()%>" /> <input type="submit"
						value="Szczegóły" />
				</form>
			</td>


		</tr>
		<%
			}
		%>
	</table>


	<a href="new-proposition">Dodaj wniosek</a>
	<br>
	<a href="index">Wróć</a>





</body>
</html>